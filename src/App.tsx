import Filters from "./components/filters";
import {Head} from "./components/head";
import {CurrentWeather} from "./components/current-weather";
import {Forecast} from "./components/forecast";
import React from "react";
import { WeatherDay } from "./model/weather"
import { Day } from "./components/day";
import store from "./model/store";
import { observer } from "mobx-react";


const App = () => {
  const weatherStore = store

  const renderWeatherDay = (day: WeatherDay) => (
    day.attrs ? <Day onClick={weatherStore.setCurrentDay} key={day.attrs.id} { ...day.attrs } /> : null
  )

  if (!weatherStore.weather || !weatherStore.currentDay) {
    return <div>Loading...</div>
  }

  return (
      <main>
        <Filters applyFilters={weatherStore.applyFilters}/>
        <Head day={weatherStore.currentDay.day}/>
        <CurrentWeather { ...weatherStore.currentDay }/>
        <Forecast>
            <> { weatherStore.days?.slice(0, 7).map(renderWeatherDay) } </>
        </Forecast>
      </main>
  );
};


export default observer(App);
