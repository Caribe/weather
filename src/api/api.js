/* Core */
import axios from "axios";
import {Weather} from "../model/weather";

const WEATHER_API_URL = process.env.REACT_APP_WEATHER_API_URL;
//
// export const api = {
//     async getWeather() {
//         const response = await axios.get(`${WEATHER_API_URL}`);
//         return new Weather(response.data);
//     },
// };


export const api = {
  async getWeather() {
    const response = await axios.get(`${WEATHER_API_URL}`);
    return response.data;
  },
};
