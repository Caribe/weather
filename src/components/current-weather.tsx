import React from "react";

type currentDayProps = {
    id: string,
    rain_probability: number,
    humidity: number,
    day: number,
    temperature: number,
    type: string,
}

export const CurrentWeather = (currentDay: currentDayProps) => {

    if (!currentDay) {
        return <div> "Loading..." </div>
    }

    return (
        <div className="current-weather">
            <p className="temperature">{currentDay.temperature}</p>
            <p className="meta">
                <span className="rainy">{currentDay.rain_probability}</span>
                <span className="humidity">{currentDay.humidity}</span>
            </p>
        </div>
    )
}