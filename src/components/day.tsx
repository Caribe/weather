import React  from "react";
import { WeatherDayAttrs } from "../model/store";
import { Instance } from "mobx-state-tree";


export type DayProps = {
    id: string,
    rain_probability: number,
    humidity: number,
    day: number,
    temperature: number,
    type: string,
    onClick: (attrs: Omit<Instance<typeof WeatherDayAttrs>, symbol>) => void
}


export const Day = (props: DayProps) => {
    const date = new Date(props.day)
    const { onClick } = props

    const fmtDate = new Intl.DateTimeFormat('ru', { weekday: 'long'} ).format(date)

    const onClickHandler = () => { onClick(props) }

    return (
        <div onClick={onClickHandler} className={ `day ${props.type}` }>
            <p>{fmtDate}</p>
            <span>{props.temperature}</span>
        </div>
    )
}