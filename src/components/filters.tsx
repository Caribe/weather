import { Formik, Field, Form } from 'formik';
import { Weather } from "../model/weather";
import { FilterAttrs } from "../model/store";
import { observer } from "mobx-react";


type Props = {
  applyFilters: (filters: FilterAttrs) => void
}

const Filters = ({ applyFilters }: Props) => {
    return (
      <div>
      <Formik
        initialValues={{
          cloudy: true,
          sunny: true,
          minTemperature: '',
          maxTemperature: ''
        }}
        onSubmit={(values) => {
          applyFilters(values)
        }}
      >
        {({values}) => (
          <Form className="filter">
            <span className="checkbox">
              Облачно
              <Field type="checkbox" name="cloudy"/>
            </span>
          <span className="checkbox">
            Солнечно
            <Field type="checkbox" name="sunny"/>
          </span>
          <p className="custom-input">
            <label htmlFor="min-temperature">Минимальная температура</label>
            <Field id="min-temperature" name="minTemperature" type="text"/>
          </p>
          <p className="custom-input">
            <label htmlFor="min-temperature">Максимальная температура</label>
            <Field id="max-temperature" name="maxTemperature" type="text"/>
          </p>
          <button type="submit">Отфильтровать</button>
        </Form>
        )}
        </Formik>
      </div>
    )
}

export default observer(Filters);