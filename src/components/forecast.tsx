import React, { ReactElement } from "react";
import { observer } from "mobx-react";

type ForecastProps = {
    children: ReactElement;
}
export const Forecast = observer(({ children }: ForecastProps) => {
    return (
        <div className="forecast">
            { children }
        </div>
    )
})