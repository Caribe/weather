import { WeatherDayAttrs } from "../model/weather";


type HeadProps = Pick<WeatherDayAttrs, "day">

export const Head = ({ day }: HeadProps) => {
    const date = new Date(day)
    const fmtWeekDate = new Intl.DateTimeFormat('ru', { weekday: 'long'} ).format(date)
    const fmtDate = new Intl.DateTimeFormat('ru', { day: '2-digit', month: 'long'} ).format(date)

    return (
        <div className="head">
            <div className="icon cloudy"/>
            <div className="current-date">
                <p>{fmtWeekDate}</p>
                <span>{fmtDate}</span>
            </div>
        </div>
    )
}
