import React, { createContext, useEffect, useState } from "react";
import { Weather, WeatherDayAttrs } from "../model/weather";
import { api } from "../api";

type ContextProps = {
    weather: Weather;
    currentDay: WeatherDayAttrs | null;
    setCurrentDay: (attr: WeatherDayAttrs | null) => void;
}

export const Context = createContext<ContextProps>({
    weather: new Weather({data: []}),
    currentDay: null,
    setCurrentDay: () => {}
});

export const WeatherProvider: React.FC = (props) => {
    const [weather, setWeather] = useState(new Weather({data: []}));
    const [currentDay, setCurrentDay] = useState<WeatherDayAttrs | null>(null);

    const getWeather = async () => {
        if(weather.days.length > 0) return;
        const data = await api.getWeather();
        setWeather(data);
        setCurrentDay(data.days[0].attrs)
    }

    useEffect(() => {
        void getWeather()
    })


    return <Context.Provider value={{weather, currentDay, setCurrentDay}}>{props.children}</Context.Provider>;
}

