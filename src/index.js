/* Core */
import { render } from 'react-dom';

/* Components */
import App from './App';

/* Instruments */
import './theme/index.scss';
import {WeatherProvider} from "./context/weather-provider";


render(
  // <Provider store={Store}>
  <App />,
  document.getElementById('root'));
