import { action, makeAutoObservable, observable } from "mobx";
import { Weather, WeatherDayAttrs } from "../weather";
import { api } from "../../api";


class WeatherStore {
  @observable weather: Weather | undefined;
  @observable currentDay: WeatherDayAttrs | undefined;

  constructor() {
    makeAutoObservable(this);
  }

  fetchData = async () => {
    if (this.weather && this.weather.days.length > 0) return
    const data = await api.getWeather();
    this.setWeather(data);
    this.setCurrentDay(data.days[0].attrs);
  }

  @action.bound setCurrentDay = (day: WeatherDayAttrs) => {
    this.currentDay = day;
  }

  @action.bound setWeather = (weather: Weather) => {
    this.weather = weather;
  }
}

export default new WeatherStore();