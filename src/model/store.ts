import { types, flow, Instance } from "mobx-state-tree";
import { api } from "../api";
import { get } from "mobx";


export const WeatherDayAttrs = types.model("WeatherDayAttrs", {
  id:               types.string,
  rain_probability: types.number,
  humidity:         types.number,
  day:              types.number,
  temperature:      types.number,
  type:             types.string,
})

const WeatherDay = types.model("WeatherDay", {
  attrs: WeatherDayAttrs,
})


const Weather = types.model("Weather", {
  days: types.array(WeatherDay),
})

export type FilterAttrs = {
  sunny: boolean,
  cloudy: boolean,
  minTemperature?: string,
  maxTemperature?: string
}

export const WeatherStore = types.model("WeatherStore", {
  weather: types.maybe(Weather),
  currentDay: types.maybe(WeatherDayAttrs),
  sunny: true,
  cloudy: true,
  minTemperature: types.maybe(types.integer),
  maxTemperature: types.maybe(types.integer)
}).actions(self => {
  const setCurrentDay = (attrs: Omit<Instance<typeof WeatherDayAttrs>, symbol>) => {
    self.currentDay = { ...attrs }
  }

  const fetchData = flow(function* () {
    if (self.weather && self.weather.days.length > 0) return
    const data = yield api.getWeather();
    const days = data.data.map((weatherAttr: Omit<Instance<typeof WeatherDayAttrs>, symbol>) => { return WeatherDay.create({attrs: weatherAttr}) })
    self.weather = Weather.create({ days: days });
    setCurrentDay(days[0].attrs);
  })

  const applyFilters = ({ sunny, cloudy, minTemperature, maxTemperature }: FilterAttrs) => {
    self.sunny = sunny;
    self.cloudy = cloudy;
    self.maxTemperature = Number(maxTemperature);
    self.minTemperature = Number(minTemperature);
  }

  return {
    fetchData,
    setCurrentDay,
    applyFilters,

    afterCreate() {
      void fetchData()
    }
  }
}).views(self => {
  return {
    get days() {
      if (!self.weather) return null
      return self.weather.days.filter(day => {
        return day.attrs.temperature > (self.minTemperature || -255)
      }).filter(day => {
        return day.attrs.temperature < (self.maxTemperature || 255)
      }).filter(day => {
        return self.sunny ? true : day.attrs.type !== "sunny"
      }).filter(day => {
        return self.cloudy ? true : day.attrs.type !== "cloudy"
      })
    }
  }
})

export default WeatherStore.create();
