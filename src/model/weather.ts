export type WeatherDayAttrs = {
    id: string;
    rain_probability: number;
    humidity: number;
    day: number;
    temperature: number;
    type: string;
}

type WeatherAttrs = {
    data: WeatherDayAttrs[]
}

export class WeatherDay {
    attrs: WeatherDayAttrs;

    constructor(data: WeatherDayAttrs) {
        this.attrs = data
    }
}

export class Weather {
    days: WeatherDay[]

    constructor({ data }: WeatherAttrs) {
        this.days = data.map<WeatherDay>((weatherAttr: WeatherDayAttrs) => { return new WeatherDay(weatherAttr) })
    }
}